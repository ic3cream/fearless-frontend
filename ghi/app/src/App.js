import logo from './logo.svg';
import './App.css';
import Nav from './Nav';

function App(props) {
  return (
    <>
      <Nav />
      <div className="container-fluid">
        <div className="table-responsive">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Conference</th>
              </tr>
            </thead>
            <tbody>
              {props.attendees && props.attendees.map(attendee => {
                return (
                  <tr key={attendee.href}>
                    <td>{ attendee.name }</td>
                    <td>{ attendee.conference }</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}

export default App;
