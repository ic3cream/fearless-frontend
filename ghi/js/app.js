function createCard(name, description, pictureUrl,starts,ends,location) {
    let formattedStartDate = new Date(starts).toLocaleDateString();
    let formattedEndDate = new Date(ends).toLocaleDateString();

    return `
      <div class="col-4"> <!-- or other col sizes based on your needs -->
        <div class="card shadow-lg mb-3">
          <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class=" card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">
          <small class="text-muted">${formattedStartDate}-${formattedEndDate}</small>
          </div>
        </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = details.conference.starts
            const ends = details.conference.ends
            const location = details.conference.location.name;
            const html = createCard(name, description, pictureUrl,starts,ends,location);

            const row = document.querySelector('.row');
            row.innerHTML += html;

          }
        }

      }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
    }

  });
