const url = 'http://localhost:8000/api/conferences/';
const response = await fetch(url);

if (response.ok) {
    const data = await response.json();
    const selectTag = document.getElementById('conference');
    for(let conference of data.conferences) {
        let option = document.createElement('option');
        option.value = conference.id;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
    }
}

const formTag = document.getElementById('create-presentation-form');
formTag.addEventListener('submit', async event => {
    event.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    // Get the selected conference ID
    const conferenceSelectTag = document.getElementById('conference');
    const conferenceid = conferenceSelectTag.value;

    // Use a template string to include the conferenceId in the URL
    const presentationUrl = `http://localhost:8000/api/conferences/${conferenceid}/presentations/`;
    const fetchConfig = {
        method:"post",
        body:json,
        headers: {
            'content-Type': 'application/json'
        },
    };
    const response = await fetch(presentationUrl, fetchConfig);
    if(response.ok){
        formTag.reset();
        const newPresentation = await response.json();
        console.log(newPresentation);
    }
});
